#!/bin/sh
#
# Return version
#   Input parameters: BRANCH_NAME, BUILD_NUMBER, COMMIT_HASH, VERSION_FILE (default - gradle.properties)
#       

if [ "$4" != "" ]
then
    VERSION_FILE="$4";
else
    VERSION_FILE="gradle.properties";
fi

VERSION=$(cat "$VERSION_FILE" | grep version= | sed -r 's/.*version=([0-9]{1,5}\.[0-9]{1,5}\.[0-9]{1,5}(-SNAPSHOT)?).*/\1/')
if [ "$1" = "master" ]
then
    VERSION=$(echo $VERSION | sed "s/-SNAPSHOT//")
    VERSION=$(echo $VERSION | sed "s/-rc//")
    VERSION=$(echo $VERSION | sed "s/-beta//")
    VERSION=$(echo $VERSION | sed "s/-alpha//")
fi

export FULL_VERSION=$(sh -c "echo $VERSION-$2-$(echo $3 | cut -c1-7)")

#export VERSION="$VERSION-$2"
export VERSION="$VERSION"